FROM node:7.3.0
RUN mkdir app
COPY ./ /app
EXPOSE 80
EXPOSE 8080
WORKDIR /app
RUN npm install
CMD ["node", "."]
